import "./App.css";
import { FullScreenPlaylist } from "./FullScreenPlaylist";

function App() {
  return (
    <div className="App">
      <FullScreenPlaylist />
    </div>
  );
}

export default App;
