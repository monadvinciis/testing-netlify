const SCREEN_DETAILS_REQUEST = "SCREEN_DETAILS_REQUEST";
const SCREEN_DETAILS_SUCCESS = "SCREEN_DETAILS_SUCCESS";
const SCREEN_DETAILS_FAIL = "SCREEN_DETAILS_FAIL";
const SCREEN_DETAILS_RESET = "SCREEN_DETAILS_RESET";

export function playlistCheckReducer(state = {}, action) {
  switch (action.type) {
    case "CHECK_PLAYLIST_REQUEST":
      return { loading: true };
    case "CHECK_PLAYLIST_SUCCESS":
      return { loading: false, data: action.payload };
    case "CHECK_PLAYLIST_FAIL":
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}

export function screenDetailsReducer(state = { loading: true }, action) {
  switch (action.type) {
    case SCREEN_DETAILS_REQUEST:
      return { loading: true };
    case SCREEN_DETAILS_SUCCESS:
      return { loading: false, screen: action.payload };
    case SCREEN_DETAILS_FAIL:
      return { loading: false, error: action.payload };
    case SCREEN_DETAILS_RESET:
      return {};
    default:
      return state;
  }
}
