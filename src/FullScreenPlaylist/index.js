import { useEffect, useState } from "react";
import "./ss.css";

// Define the main component
export const FullScreenPlaylist = () => {
  const [index, setIndex] = useState(0);
  const medias = [
    "https://ipfs.io/ipfs/bafybeigf2lwr4uoeswyqhndwlczmkbwpgvo6m5gge3jlkqwti3anpvzaxa",
    "https://ipfs.io/ipfs/bafybeidimnwmntox353t3xx67suredveessvkpvumxkfiryv5xjle35dvi",
  ];

  function onEnd(e) {
    e.preventDefault();
    console.log("onEnd function aca]aaa", (index + 1) % 2);
    setIndex((index + 1) % 2);
  }
  useEffect(() => {
    console.log("index cahnages");
  }, [index]);

  return (
    <div
      // allowFullScreen
      id="myVideo"
      height="100%"
      align="center"
      // __css={style}
    >
      <video
        id="myvideo"
        controls
        autoplay
        onEnded={(e) => onEnd(e)}
        style={{ height: "100%", width: "100%" }}
      >
        <source src={medias[index]} type="video/mp4" />
      </video>
    </div>
  );
};
