import Axios from "axios";

const SCREEN_DETAILS_REQUEST = "SCREEN_DETAILS_REQUEST";
const SCREEN_DETAILS_SUCCESS = "SCREEN_DETAILS_SUCCESS";
const SCREEN_DETAILS_FAIL = "SCREEN_DETAILS_FAIL";

// screen details '''''''''''''''''''''''
export const detailsScreen = (screenId) => async (dispatch) => {
  dispatch({
    type: SCREEN_DETAILS_REQUEST,
    payload: screenId,
  });
  try {
    const { data } = await Axios.get(
      `${process.env.REACT_APP_BLINDS_SERVER}/api/screens/${screenId}`
    );
    dispatch({
      type: SCREEN_DETAILS_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: SCREEN_DETAILS_FAIL,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message,
    });
  }
};

// test  ''''''''''''''''''''''''''
export const checkPlaylist =
  ({ screenName, timeNow, currentVid, deviceInfo }) =>
  async (dispatch) => {
    dispatch({
      type: "CHECK_PLAYLIST_REQUEST",
      payload: currentVid,
    });
    try {
      const { data } = await Axios.get(
        `${process.env.REACT_APP_BLINDS_SERVER}/api/screens/${screenName}/screenName/${timeNow}/${currentVid}`,
        {
          params: {
            currentVid,
            deviceInfo,
          },
        }
      );
      dispatch({
        type: "CHECK_PLAYLIST_SUCCESS",
        payload: data,
      });
    } catch (error) {
      dispatch({
        type: "CHECK_PLAYLIST_FAIL",
        payload:
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message,
      });
    }
  };
